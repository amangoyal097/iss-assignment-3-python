# Iss Assignment 3
Assignment 3 of Iss
Python game

**Movement Instructions**

Both players are assigned arrow keys to move

**Design Choices**

1. Every partition gives you 10 points
2. Both players are given 3 additional lives to play
3. Level for a player increases after completion
4. Player has to complete the round in the given time
5. Every second saved gives increments the score by 2
6. If a player dies it is respawned if it has remaining lives
7. When player is respawned after collided time is decremented for him as pelenty
8. If a player completes , the next player plays if it is ingame
9. If a player completes and next player is out then, the player who completed is respawned to next level
10. after both players have lost all their lives the game ends
11. The player with the higher score wins